#pragma once

#include <atlstr.h>
#include <mmsystem.h>
#pragma comment(lib, "winmm.lib")

#include <sndfile.h>

#include <iostream>
#include <fstream>

#include <cstring>
#include <cstdlib>

#include <vamp-hostsdk/PluginHostAdapter.h>
#include <vamp-hostsdk/PluginInputDomainAdapter.h>
#include <vamp-hostsdk/PluginLoader.h>

#include <msclr/marshal_cppstd.h>

namespace SoundEmo {

	using namespace System;
	using namespace System::IO;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Collections::Generic;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Threading;

	using Vamp::Plugin;
	using Vamp::PluginHostAdapter;
	using Vamp::RealTime;
	using Vamp::HostExt::PluginLoader;
	using Vamp::HostExt::PluginWrapper;
	using Vamp::HostExt::PluginInputDomainAdapter;

	/// <summary>
	/// Summary for GUI
	/// </summary>
	public ref class GUI : public System::Windows::Forms::Form
	{
	public:
		GUI(void)
		{
			InitializeComponent();
			this->progressWorker = gcnew BackgroundWorker();
			this->progressWorker->DoWork += gcnew DoWorkEventHandler(this, &GUI::workerDoWork);
			this->progressWorker->ProgressChanged += gcnew ProgressChangedEventHandler(this, &GUI::workerProgressChanged);
			this->progressWorker->WorkerReportsProgress = true;
			this->progressWorker->WorkerSupportsCancellation = true;
			this->progressWorker->RunWorkerAsync();
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~GUI()
		{
			end = true;
			this->progressWorker->CancelAsync();
			if (components)
			{
				delete components;
			}
		}

	private: bool end = false;
	protected: BackgroundWorker ^progressWorker;

	private: System::Windows::Forms::Panel^ panel1;
	private: System::Windows::Forms::TrackBar^ trackBar1;
	private: System::Windows::Forms::Button^ button2;
	private: System::Windows::Forms::Panel^ panel2;
	private: System::Windows::Forms::TextBox^ textBox1;
	private: System::Windows::Forms::Button^ button1;
	private: System::Windows::Forms::Panel^  panel3;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->panel2 = (gcnew System::Windows::Forms::Panel());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->trackBar1 = (gcnew System::Windows::Forms::TrackBar());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->panel3 = (gcnew System::Windows::Forms::Panel());
			this->panel1->SuspendLayout();
			this->panel2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar1))->BeginInit();
			this->SuspendLayout();
			// 
			// panel1
			// 
			this->panel1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->panel1->Controls->Add(this->button1);
			this->panel1->Controls->Add(this->panel2);
			this->panel1->Controls->Add(this->trackBar1);
			this->panel1->Controls->Add(this->button2);
			this->panel1->Location = System::Drawing::Point(0, 407);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(592, 66);
			this->panel1->TabIndex = 4;
			// 
			// button1
			// 
			this->button1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left));
			this->button1->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->button1->Location = System::Drawing::Point(12, 12);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(42, 42);
			this->button1->TabIndex = 5;
			this->button1->Text = L"open";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &GUI::button1_Click_1);
			// 
			// panel2
			// 
			this->panel2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Right));
			this->panel2->Controls->Add(this->textBox1);
			this->panel2->Location = System::Drawing::Point(503, 12);
			this->panel2->Name = L"panel2";
			this->panel2->Size = System::Drawing::Size(77, 42);
			this->panel2->TabIndex = 4;
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(3, 8);
			this->textBox1->Name = L"textBox1";
			this->textBox1->ReadOnly = true;
			this->textBox1->Size = System::Drawing::Size(71, 20);
			this->textBox1->TabIndex = 0;
			this->textBox1->Text = L"\? / \?";
			this->textBox1->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// trackBar1
			// 
			this->trackBar1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Left | System::Windows::Forms::AnchorStyles::Right));
			this->trackBar1->Location = System::Drawing::Point(108, 12);
			this->trackBar1->Maximum = 200;
			this->trackBar1->Name = L"trackBar1";
			this->trackBar1->Size = System::Drawing::Size(389, 42);
			this->trackBar1->TabIndex = 0;
			this->trackBar1->TabStop = false;
			this->trackBar1->Scroll += gcnew System::EventHandler(this, &GUI::trackBar1_Scroll);
			// 
			// button2
			// 
			this->button2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left));
			this->button2->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->button2->Location = System::Drawing::Point(60, 12);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(42, 42);
			this->button2->TabIndex = 2;
			this->button2->Text = L">";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &GUI::button2_Click);
			// 
			// panel3
			// 
			this->panel3->Location = System::Drawing::Point(13, 13);
			this->panel3->Name = L"panel3";
			this->panel3->Size = System::Drawing::Size(564, 388);
			this->panel3->TabIndex = 5;
			// 
			// GUI
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(592, 473);
			this->Controls->Add(this->panel3);
			this->Controls->Add(this->panel1);
			this->MaximumSize = System::Drawing::Size(600, 500);
			this->MinimumSize = System::Drawing::Size(600, 500);
			this->Name = L"GUI";
			this->Text = L"SoundEmo";
			this->panel1->ResumeLayout(false);
			this->panel1->PerformLayout();
			this->panel2->ResumeLayout(false);
			this->panel2->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar1))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion

		public: enum class ChordType : char {
			C, c, Cis, cis, D, d, Dis, dis, E, e, F, f, Fis, fis, G, g, Gis, gis, A, a, B, b, H, h
		};

		ref class TimeChord {
		public:
			double time;
			ChordType chord;
		};

		private: LPCWSTR convert(String ^string) {
			CString cstring(string);
			return cstring;
		}

		private: int mciFrameCount = 0;

		private: int getMciFrameCount() {
			WCHAR data[100];
			mciSendStringW(L"status MY_SND length", data, 100, NULL);

			//OutputDebugStringW(convert("frame count: "));
			//OutputDebugStringW(data);
			//OutputDebugStringW(convert("\n"));

			return _tstoi(data);
		}

		private: int getMciFrame() {
			WCHAR data[100];
			mciSendStringW(L"status MY_SND position", data, 100, NULL);

			return _tstoi(data);
		}

		///////////////////////////////////////////////////////////////////////////////////////////

		void workerProgressChanged(Object ^sender, ProgressChangedEventArgs ^e) {
			if (end) {
				OutputDebugStringW(L"escaped exception!\n");
				return;
			}
			this->Text = L"rtime: " + e->ProgressPercentage;

			OutputDebugStringW(convert("frame: " + getMciFrame() + "\n"));

			double current_time = ((double)getMciFrame())/1000;
			OutputDebugStringW(convert("time: " + current_time + "\n"));
			if (tc && tc->Count > 0) {
				double time = tc[0]->time;
				ChordType chord = tc[0]->chord;
				for (int i = 1; i < tc->Count; i++) {
					if (abs(current_time - time) > abs(current_time - tc[i]->time)) {
						time = tc[i]->time;
						chord = tc[i]->chord;
					}
				}
				this->drawMood(chord);
			}

			if (mciFrameCount != 0) {
				this->trackBar1->Value = this->trackBar1->Maximum * getMciFrame() / mciFrameCount;
			} else {
				this->trackBar1->Value = 0;
			}

			int max_seconds = mciFrameCount / 1000;
			int current_seconds = (int)current_time;
			char timer[50];
			sprintf_s(timer, "%d:%02d / %d:%02d", current_seconds / 60, current_seconds % 60, max_seconds / 60, max_seconds % 60);
			System::String^ timerString = gcnew System::String(timer);
			this->textBox1->Text = timerString;
		}

		void workerDoWork(Object ^sender, DoWorkEventArgs ^e) {
			int i = 0;
			while (1) {
				Thread::Sleep(500);
				progressWorker->ReportProgress(i);
				i++;
			}
		}

		///////////////////////////////////////////////////////////////////////////////////////////

		private: List<TimeChord^>^ tc;

		private: System::Void button1_Click_1(System::Object^ sender, System::EventArgs^ e) {
			OpenFileDialog ^ openFileDialog1 = gcnew OpenFileDialog();
			openFileDialog1->Filter = "Waveform Audio File|*.wav";
			openFileDialog1->Title = "Select a sound file";

			if (openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK) {
				mciSendString(L"close MY_SND", NULL, 0, NULL);
				mciFrameCount = 0;

				tc = produceChords(msclr::interop::marshal_as< std::string >(openFileDialog1->FileName));

				mciSendString(convert("open " + openFileDialog1->FileName + " alias MY_SND"), NULL, 0, NULL);
				mciSendString(convert("set " + openFileDialog1->FileName + " time format milliseconds"), NULL, 0, NULL);

				mciFrameCount = getMciFrameCount();
				
				play();
				mciSendString(L"set MY_SND seek exactly on", NULL, 0, NULL);

				//PlaySound(convert(openFileDialog1->FileName), NULL, SND_FILENAME || SND_ASYNC);
			}
		}

		private: bool playing = false;

		private: void play() {
			mciSendStringW(L"play MY_SND", NULL, 0, NULL);
			mciSendStringW(L"resume MY_SND", NULL, 0, NULL);
			this->button2->Text = L"| |";
			playing = true;
		}

		private: void pause() {
			mciSendStringW(L"pause MY_SND", NULL, 0, NULL);
			this->button2->Text = L">";
			playing = false;
		}

		private: System::Void button2_Click(System::Object^ sender, System::EventArgs^ e) {
			if (playing) {
				pause();
			} else {
				play();
			}
		}

		private: System::Void trackBar1_Scroll(System::Object^  sender, System::EventArgs^  e) {
			int mciFrame = this->trackBar1->Value * mciFrameCount / this->trackBar1->Maximum;
			mciSendString(convert("seek MY_SND to " + mciFrame), NULL, 0, NULL);
			play();
		}

		///////////////////////////////////////////////////////////////////////////////////////////

		private: System::Void drawMood(ChordType mood) {
			HDC hdc;
			HWND hWnd = static_cast<HWND>(panel3->Handle.ToPointer());
			hdc = GetDC(hWnd);
			switch (mood) {
			case ChordType::C:	panel3->BackColor = System::Drawing::Color::AliceBlue; break;
			case ChordType::c: panel3->BackColor = System::Drawing::Color::DarkBlue; break;
			case ChordType::Cis: panel3->BackColor = System::Drawing::Color::Cyan; break;
			case ChordType::cis: panel3->BackColor = System::Drawing::Color::DarkCyan; break;
			case ChordType::D: panel3->BackColor = System::Drawing::Color::Green; break;
			case ChordType::d: panel3->BackColor = System::Drawing::Color::DarkGreen; break;
			case ChordType::Dis: panel3->BackColor = System::Drawing::Color::Magenta; break;
			case ChordType::dis: panel3->BackColor = System::Drawing::Color::DarkMagenta; break;
			case ChordType::E: panel3->BackColor = System::Drawing::Color::Violet; break;
			case ChordType::e: panel3->BackColor = System::Drawing::Color::DarkViolet; break;
			case ChordType::F: panel3->BackColor = System::Drawing::Color::Orange; break;
			case ChordType::f: panel3->BackColor = System::Drawing::Color::DarkOrange; break;
			case ChordType::Fis: panel3->BackColor = System::Drawing::Color::Orchid; break;
			case ChordType::fis: panel3->BackColor = System::Drawing::Color::DarkOrchid; break;
			case ChordType::G: panel3->BackColor = System::Drawing::Color::Red; break;
			case ChordType::g: panel3->BackColor = System::Drawing::Color::DarkRed; break;
			case ChordType::Gis: panel3->BackColor = System::Drawing::Color::Salmon; break;
			case ChordType::gis: panel3->BackColor = System::Drawing::Color::DarkSalmon; break;
			case ChordType::A: panel3->BackColor = System::Drawing::Color::SeaGreen; break;
			case ChordType::a: panel3->BackColor = System::Drawing::Color::DarkSeaGreen; break;
			case ChordType::B: panel3->BackColor = System::Drawing::Color::Goldenrod; break;
			case ChordType::b: panel3->BackColor = System::Drawing::Color::DarkGoldenrod; break;
			case ChordType::H: panel3->BackColor = System::Drawing::Color::SlateBlue; break;
			case ChordType::h: panel3->BackColor = System::Drawing::Color::DarkSlateBlue; break;
			default: panel3->BackColor = System::Drawing::Color::Ivory; break;
			}
			Refresh();
		}

		///////////////////////////////////////////////////////////////////////////////////////////

		static void printFeatures(int frame, int sr, int output, Plugin::FeatureSet features, std::ofstream *out, bool useFrames) {
			for (unsigned int i = 0; i < features[output].size(); ++i) {

				if (useFrames) {

					int displayFrame = frame;

					if (features[output][i].hasTimestamp) {
						displayFrame = RealTime::realTime2Frame
							(features[output][i].timestamp, sr);
					}

					(out ? *out : std::cout) << displayFrame;

					if (features[output][i].hasDuration) {
						displayFrame = RealTime::realTime2Frame
							(features[output][i].duration, sr);
						(out ? *out : std::cout) << "," << displayFrame;
					}

					(out ? *out : std::cout) << ":";

				}
				else {

					RealTime rt = RealTime::frame2RealTime(frame, sr);

					if (features[output][i].hasTimestamp) {
						rt = features[output][i].timestamp;
					}

					(out ? *out : std::cout) << rt.toString();

					if (features[output][i].hasDuration) {
						rt = features[output][i].duration;
						(out ? *out : std::cout) << "," << rt.toString();
					}

					(out ? *out : std::cout) << ":";
				}

				for (unsigned int j = 0; j < features[output][i].values.size(); ++j) {
					(out ? *out : std::cout) << " " << features[output][i].values[j];
				}
				(out ? *out : std::cout) << " " << features[output][i].label;

				(out ? *out : std::cout) << std::endl;
			}
		}


		static int runPlugin(std::string myname, std::string soname, std::string id, std::string output, int outputNo, std::string wavname, std::string outfilename, bool useFrames)
		{
			PluginLoader *loader = PluginLoader::getInstance();

			PluginLoader::PluginKey key = loader->composePluginKey(soname, id);

			SNDFILE *sndfile;
			SF_INFO sfinfo;
			memset(&sfinfo, 0, sizeof(SF_INFO));

			sndfile = sf_open(wavname.c_str(), SFM_READ, &sfinfo);
			if (!sndfile) {
				std::cerr << myname << ": ERROR: Failed to open input file \""
					<< wavname << "\": " << sf_strerror(sndfile) << std::endl;
				return 1;
			}

			std::ofstream *out = 0;
			if (outfilename != "") {
				out = new std::ofstream(outfilename.c_str(), std::ios::out);
				if (!*out) {
					std::cerr << myname << ": ERROR: Failed to open output file \""
						<< outfilename << "\" for writing" << std::endl;
					delete out;
					return 1;
				}
			}

			Plugin *plugin = loader->loadPlugin
				(key, sfinfo.samplerate, PluginLoader::ADAPT_ALL_SAFE);
			if (!plugin) {
				std::cerr << myname << ": ERROR: Failed to load plugin \"" << id
					<< "\" from library \"" << soname << "\"" << std::endl;
				sf_close(sndfile);
				if (out) {
					out->close();
					delete out;
				}
				return 1;
			}

			std::cerr << "Running plugin: \"" << plugin->getIdentifier() << "\"..." << std::endl;

			// Note that the following would be much simpler if we used a
			// PluginBufferingAdapter as well -- i.e. if we had passed
			// PluginLoader::ADAPT_ALL to loader->loadPlugin() above, instead
			// of ADAPT_ALL_SAFE.  Then we could simply specify our own block
			// size, keep the step size equal to the block size, and ignore
			// the plugin's bleatings.  However, there are some issues with
			// using a PluginBufferingAdapter that make the results sometimes
			// technically different from (if effectively the same as) the
			// un-adapted plugin, so we aren't doing that here.  See the
			// PluginBufferingAdapter documentation for details.

			int blockSize = plugin->getPreferredBlockSize();
			int stepSize = plugin->getPreferredStepSize();

			if (blockSize == 0) {
				blockSize = 1024;
			}
			if (stepSize == 0) {
				if (plugin->getInputDomain() == Plugin::FrequencyDomain) {
					stepSize = blockSize / 2;
				}
				else {
					stepSize = blockSize;
				}
			}
			else if (stepSize > blockSize) {
				std::cerr << "WARNING: stepSize " << stepSize << " > blockSize " << blockSize << ", resetting blockSize to ";
				if (plugin->getInputDomain() == Plugin::FrequencyDomain) {
					blockSize = stepSize * 2;
				}
				else {
					blockSize = stepSize;
				}
				std::cerr << blockSize << std::endl;
			}
			int overlapSize = blockSize - stepSize;
			sf_count_t currentStep = 0;
			int finalStepsRemaining = max(1, (blockSize / stepSize) - 1); // at end of file, this many part-silent frames needed after we hit EOF

			int channels = sfinfo.channels;

			float *filebuf = new float[blockSize * channels];
			float **plugbuf = new float*[channels];
			for (int c = 0; c < channels; ++c) plugbuf[c] = new float[blockSize + 2];

			std::cerr << "Using block size = " << blockSize << ", step size = "
				<< stepSize << std::endl;

			// The channel queries here are for informational purposes only --
			// a PluginChannelAdapter is being used automatically behind the
			// scenes, and it will take case of any channel mismatch

			int minch = plugin->getMinChannelCount();
			int maxch = plugin->getMaxChannelCount();
			std::cerr << "Plugin accepts " << minch << " -> " << maxch << " channel(s)" << std::endl;
			std::cerr << "Sound file has " << channels << " (will mix/augment if necessary)" << std::endl;

			Plugin::OutputList outputs = plugin->getOutputDescriptors();
			Plugin::OutputDescriptor od;

			int returnValue = 1;
			int progress = 0;

			RealTime rt;
			PluginWrapper *wrapper = 0;
			RealTime adjustment = RealTime::zeroTime;

			if (outputs.empty()) {
				std::cerr << "ERROR: Plugin has no outputs!" << std::endl;
				goto done;
			}

			if (outputNo < 0) {

				for (size_t oi = 0; oi < outputs.size(); ++oi) {
					if (outputs[oi].identifier == output) {
						outputNo = oi;
						break;
					}
				}

				if (outputNo < 0) {
					std::cerr << "ERROR: Non-existent output \"" << output << "\" requested" << std::endl;
					goto done;
				}

			}
			else {

				if (int(outputs.size()) <= outputNo) {
					std::cerr << "ERROR: Output " << outputNo << " requested, but plugin has only " << outputs.size() << " output(s)" << std::endl;
					goto done;
				}
			}

			od = outputs[outputNo];
			std::cerr << "Output is: \"" << od.identifier << "\"" << std::endl;

			if (!plugin->initialise(channels, stepSize, blockSize)) {
				std::cerr << "ERROR: Plugin initialise (channels = " << channels
					<< ", stepSize = " << stepSize << ", blockSize = "
					<< blockSize << ") failed." << std::endl;
				goto done;
			}

			wrapper = dynamic_cast<PluginWrapper *>(plugin);
			if (wrapper) {
				// See documentation for
				// PluginInputDomainAdapter::getTimestampAdjustment
				PluginInputDomainAdapter *ida =
					wrapper->getWrapper<PluginInputDomainAdapter>();
				if (ida) adjustment = ida->getTimestampAdjustment();
			}

			// Here we iterate over the frames, avoiding asking the numframes in case it's streaming input.
			do {

				int count;

				if ((blockSize == stepSize) || (currentStep == 0)) {
					// read a full fresh block
					if ((count = sf_readf_float(sndfile, filebuf, blockSize)) < 0) {
						std::cerr << "ERROR: sf_readf_float failed: " << sf_strerror(sndfile) << std::endl;
						break;
					}
					if (count != blockSize) --finalStepsRemaining;
				}
				else {
					//  otherwise shunt the existing data down and read the remainder.
					memmove(filebuf, filebuf + (stepSize * channels), overlapSize * channels * sizeof(float));
					if ((count = sf_readf_float(sndfile, filebuf + (overlapSize * channels), stepSize)) < 0) {
						std::cerr << "ERROR: sf_readf_float failed: " << sf_strerror(sndfile) << std::endl;
						break;
					}
					if (count != stepSize) --finalStepsRemaining;
					count += overlapSize;
				}

				for (int c = 0; c < channels; ++c) {
					int j = 0;
					while (j < count) {
						plugbuf[c][j] = filebuf[j * sfinfo.channels + c];
						++j;
					}
					while (j < blockSize) {
						plugbuf[c][j] = 0.0f;
						++j;
					}
				}

				rt = RealTime::frame2RealTime(currentStep * stepSize, sfinfo.samplerate);

				printFeatures
					(RealTime::realTime2Frame(rt + adjustment, sfinfo.samplerate),
						sfinfo.samplerate, outputNo, plugin->process(plugbuf, rt),
						out, useFrames);

				if (sfinfo.frames > 0) {
					int pp = progress;
					progress = (int)((float(currentStep * stepSize) / sfinfo.frames) * 100.f + 0.5f);
					if (progress != pp && out) {
						std::cerr << "\r" << progress << "%";
					}
				}

				++currentStep;

			} while (finalStepsRemaining > 0);

			if (out) std::cerr << "\rDone" << std::endl;

			rt = RealTime::frame2RealTime(currentStep * stepSize, sfinfo.samplerate);

			printFeatures(RealTime::realTime2Frame(rt + adjustment, sfinfo.samplerate),
				sfinfo.samplerate, outputNo,
				plugin->getRemainingFeatures(), out, useFrames);

			returnValue = 0;

		done:
			delete plugin;
			if (out) {
				out->close();
				delete out;
			}
			sf_close(sndfile);
			return returnValue;
		}

		public: static List<TimeChord^>^ produceChords(std::string inputWavFilename) {

			runPlugin("name", "hpa", "HPA_LBC", "wyjscie", 0, inputWavFilename, "", false);
			runPlugin("name", "hpa", "HPA", "wyjscie", 0, inputWavFilename, "chords", false);

			StreamReader ^sr = gcnew StreamReader("chords");

			// Read the stream to a string, and write the string to the console.
			String ^line = sr->ReadToEnd();
			array<String^> ^splitted = line->Split(' ');

			List<TimeChord^> ^result = gcnew List<TimeChord^>(splitted->Length / 2);


			for (int i = 1; i < splitted->Length; i++) {

				TimeChord ^tc = gcnew TimeChord();

				String ^strTime = splitted[i];
				strTime = strTime->Remove(strTime->Length - 1)->Replace(".", ",");
				tc->time = Double::Parse(strTime);

				i = i + 1;
				ChordType readChord;

				String^ toCompare = splitted[i]->Trim();

				if (toCompare->Equals("C:maj")) {
					readChord = ChordType::C;
				}
				else if (toCompare->Equals("C:min")) {
					readChord = ChordType::c;
				}
				else if (toCompare->Equals("Db:maj/C#")) {
					readChord = ChordType::Cis;
				}
				else if (toCompare->Equals("Db:min/C#")) {
					readChord = ChordType::cis;
				}
				else if (toCompare->Equals("D:maj")) {
					readChord = ChordType::D;
				}
				else if (toCompare->Equals("D:min")) {
					readChord = ChordType::d;
				}
				else if (toCompare->Equals("Eb:maj/D#")) {
					readChord = ChordType::Dis;
				}
				else if (toCompare->Equals("Eb:min/D#")) {
					readChord = ChordType::dis;
				}
				else if (toCompare->Equals("E:maj")) {
					readChord = ChordType::E;
				}
				else if (toCompare->Equals("E:min")) {
					readChord = ChordType::e;
				}
				else if (toCompare->Equals("F:maj")) {
					readChord = ChordType::F;
				}
				else if (toCompare->Equals("F:min")) {
					readChord = ChordType::f;
				}
				else if (toCompare->Equals("Gb:maj/F#")) {
					readChord = ChordType::Fis;
				}
				else if (toCompare->Equals("Gb:min/F#")) {
					readChord = ChordType::fis;
				}
				else if (toCompare->Equals("G:maj")) {
					readChord = ChordType::G;
				}
				else if (toCompare->Equals("G:min")) {
					readChord = ChordType::g;
				}
				else if (toCompare->Equals("Ab:maj/G#")) {
					readChord = ChordType::Gis;
				}
				else if (toCompare->Equals("Ab:min/G#")) {
					readChord = ChordType::gis;
				}
				else if (toCompare->Equals("A:maj")) {
					readChord = ChordType::A;
				}
				else if (toCompare->Equals("A:min")) {
					readChord = ChordType::a;
				}
				else if (toCompare->Equals("Bb:maj/A#")) {
					readChord = ChordType::B;
				}
				else if (toCompare->Equals("Bb:min/A#")) {
					readChord = ChordType::b;
				}
				else if (toCompare->Equals("B:maj")) {
					readChord = ChordType::H;
				}
				else if (toCompare->Equals("B:min")) {
					readChord = ChordType::h;
				}
				else {
					OutputDebugString(L"Wrong chord!");
					continue;
				}

				tc->chord = readChord;

				result->Add(tc);

			}

			return result;

		}
	};
}
